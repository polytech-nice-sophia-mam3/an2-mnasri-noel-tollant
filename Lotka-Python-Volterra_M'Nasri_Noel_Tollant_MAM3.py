from matplotlib import colors
from matplotlib import pyplot as plt
import numpy as np
import os



#=====================[ Variables globales ]======================

currentDirectoryPath = str(os.path.dirname(os.path.realpath(__file__))).replace('\\','/')
fontSize = 17
fontSizeTitle = 20
figHeight = 10
figWidth = 17

#=================================================================



#========================[ 2 populations ]========================

# Fonction renvoyant chaque membre de droite (en colonne) du système de Lotka-Volterra avec 2 populations
def FLVdP2(zn, alpha, beta, gamma, delta) :
    xn, yn = zn[0], zn[1]
    Fx = alpha * xn - beta * xn * yn
    Fy = delta * xn * yn - gamma * yn
    F = np.array([[Fx], 
                  [Fy]])
    # F est donc un vecteur de 2 lignes et 1 colonne, comme zn
    return F


# Fonction renvoyant la matrice jacobienne du système de Lotka-Volterra avec 2 populations
def JacobFLVdP2(zn, alpha, beta, gamma, delta) :
    xn, yn = zn[0], zn[1]
    DFxDx = alpha - beta * yn
    DFxDy = -beta * xn
    DFyDx = delta * yn
    DFyDy = delta * xn - gamma
    DF = np.array([[DFxDx, DFxDy], 
                   [DFyDx, DFyDy]])
    # DF est donc une matrice carrée de 2 lignes et 2 colonnes
    return DF


# Implémentation de la méthode itérative de Newton pour 2 populations
def IteNewtonP2(g, dg, z0=np.array([[0],[0]]), eps=1e-8, maxIter=1000) :
    i = 0
    zn = z0
    while i < maxIter :
        Fz = g(zn).reshape((2, 1))          # oh qu'il est important ce "reshape" !
        DFz = np.matrix(dg(zn))
        if np.linalg.det(DFz) == 0 :
            print("Erreur: DFz n'est pas inversible")
            break
        dz = np.linalg.solve(DFz, Fz)
        zn = zn - dz
        if np.linalg.norm(dz) < eps :
            break
        i += 1
    return zn


# Implémentation de la méthode d'Euler implicite pour 2 populations
def EulerImpliciteP2(alpha, beta, gamma, delta, h=0.01, z0=np.array([[0],[0]]), eps=1e-8, N=10000):
    t = np.arange(0, (N+0.5)*h, h)
    Z = np.zeros((2, N+1))
    Z[:, 0] = z0[:, 0]
    for i in range(0, N):
        zi = Z[:, i].reshape((2, 1))
        g = lambda v: -v + zi + h * FLVdP2(v, alpha, beta, gamma, delta).reshape((2, 1))
        dg = lambda v: -np.eye(2) + h * np.matrix(JacobFLVdP2(v, alpha, beta, gamma, delta))
        zj = IteNewtonP2(g, dg, zi).reshape((2, 1))
        Z[:, i+1] = zj.flatten()
    return t, Z


# Implémentation de la méthode de Runge-Kutta explicite d'ordre 2 pour 2 populations
def RungeKutta2P2(alpha, beta, gamma, delta, h=0.01, z0=np.array([[0],[0]]), N=10000):
    t = np.arange(0, (N+0.5)*h, h)
    Z = np.zeros((2, N+1))
    Z[:, 0] = z0[:, 0]
    for i in range(1, N+1) :
        zi1 = Z[:, i-1].reshape((2, 1))
        k1 = FLVdP2(zi1, alpha, beta, gamma, delta).reshape((2, 1))
        zi2 = zi1 + h/2 * k1
        k2 = FLVdP2(zi2, alpha, beta, gamma, delta).reshape((2, 1))
        Z[:, i] = (zi1 + h * k2).flatten()
    return t, Z


# Implémentation de la méthode de Runge-Kutta explicite d'ordre 4 pour 2 populations
def RungeKutta4P2(alpha, beta, gamma, delta, h=0.01, z0=np.array([[0],[0]]), N=10000):
    t = np.arange(0, (N+0.5)*h, h)
    Z = np.zeros((2, N+1))
    Z[:, 0] = z0[:, 0]
    for i in range(1, N+1) :
        zi1 = Z[:, i-1].reshape((2, 1))
        k1 = FLVdP2(zi1, alpha, beta, gamma, delta).reshape((2, 1))
        zi2 = zi1 + h/2 * k1
        k2 = FLVdP2(zi2, alpha, beta, gamma, delta).reshape((2, 1))
        zi3 = zi1 + h/2 * k2
        k3 = FLVdP2(zi3, alpha, beta, gamma, delta).reshape((2, 1))
        zi4 = zi1 + h * k3
        k4 = FLVdP2(zi4, alpha, beta, gamma, delta).reshape((2, 1))
        Z[:, i] = (zi1 + h/6 * (k1 + 2*k2 + 2*k3 + k4)).flatten()
    return t, Z


# Fonction pour tracer l'évolution des populations en fonction du temps et des populations entre elles, lorsqu'il n'y a que 2 populations
def plotResultsP2(t, z, p="", n="") :
    plt.figure()
    fig, ax = plt.subplots()
    ax.plot(t, z[0,:], label="Proies")
    ax.plot(t, z[1,:], label="Prédateurs")
    ax.set_xlabel("Temps", fontsize=fontSize)
    ax.set_ylabel("Populations", fontsize=fontSize)
    ax.set_title("Évolution des proies et des prédateurs en fonction du temps"+p, fontsize=fontSizeTitle)
    ax.xaxis.set_tick_params(labelsize=fontSize)
    ax.yaxis.set_tick_params(labelsize=fontSize)
    ax.legend(loc="upper right", fontsize=fontSize)
    fig.set_figheight(figHeight)
    fig.set_figwidth(figWidth)
    path = currentDirectoryPath + "/graphes/" + n[1] + " populations/relatif"
    os.makedirs(path, exist_ok=True)
    plt.savefig(path + "/" + n[0:2] + "Relatif" + n[2:] + ".png")
    plt.close()
    plt.figure()
    fig, ax = plt.subplots()
    ax.plot(z[0,:], z[1,:])
    ax.set_xlabel("Proies", fontsize=fontSize)
    ax.set_ylabel("Prédateurs", fontsize=fontSize)
    ax.set_title("Évolution des proies en fonction des prédateurs"+p, fontsize=fontSizeTitle)
    ax.xaxis.set_tick_params(labelsize=fontSize)
    ax.yaxis.set_tick_params(labelsize=fontSize)
    fig.set_figheight(figHeight)
    fig.set_figwidth(figWidth)
    path = currentDirectoryPath + "/graphes/" + n[1] + " populations/absolu"
    os.makedirs(path, exist_ok=True)
    plt.savefig(path + "/" + n[0:2] + "Absolu" + n[2:] + ".png")
    plt.close()

#=================================================================



#========================[ 3 populations ]========================


# Fonction renvoyant chaque membre de droite (en colonne) du système de Lotka-Volterra avec 3 populations
def FLVdP3(vn, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa) :
    xn, yn, zn = vn[0], vn[1], vn[2]
    Fx = xn * (alpha - beta * xn - gamma * yn)
    Fy = yn * (delta - epsilon * yn - zeta * xn - eta * zn)
    Fz = zn * (theta * yn - iota * zn - kappa)
    F = np.array([[Fx], 
                  [Fy],
                  [Fz]])
    # F est donc un vecteur de 3 lignes et 1 colonne, comme vn
    return F


# Fonction renvoyant la matrice jacobienne du système de Lotka-Volterra avec 3 populations
def JacobFLVdP3(vn, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa) :
    xn, yn, zn = vn[0], vn[1], vn[2]
    DF = np.zeros((3, 3))
    # DF est donc une matrice carrée de 3 lignes et 3 colonnes
    DF[0, 0] = alpha - 2 * beta * xn - gamma * yn
    DF[0, 1] = -gamma * xn
    DF[0, 2] = 0
    DF[1, 0] = -zeta * yn
    DF[1, 1] = delta - 2 * epsilon * yn - zeta * xn - eta * zn
    DF[1, 2] = -eta * yn
    DF[2, 0] = 0
    DF[2, 1] = theta * zn
    DF[2, 2] = theta * yn - 2 * iota * zn - kappa
    return DF


# Implémentation de la méthode itérative de Newton pour 3 populations
def IteNewtonP3(g, dg, v0=np.array([[0],[0],[0]]), err=1e-8, maxIter=1000) :
    i = 0
    vn = v0
    while i < maxIter :
        Fz = g(vn).reshape((3, 1))
        DFz = np.matrix(dg(vn))
        if np.linalg.det(DFz) == 0 :
            print("Erreur: DFz n'est pas inversible")
            break
        dz = np.linalg.solve(DFz, Fz)
        vn = vn - dz
        if np.linalg.norm(dz) < err :
            break
        i += 1
    return vn


# Implémentation de la méthode d'Euler implicite pour 3 populations
def EulerImpliciteP3(alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa, h=0.01, v0=np.array([[0],[0],[0]]), err=1e-8, N=10000):
    t = np.arange(0, (N+0.5)*h, h)
    V = np.zeros((3, N+1))
    V[:, 0] = v0[:, 0]
    for i in range(0, N):
        vi = V[:, i].reshape((3, 1))
        g = lambda u: -u + vi + h * FLVdP3(u, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa).reshape((3, 1))
        dg = lambda u: -np.eye(3) + h * np.matrix(JacobFLVdP3(u, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa))
        vj = IteNewtonP3(g, dg, vi).reshape((3, 1))
        V[:, i+1] = vj.flatten()
    return t, V


# Implémentation de la méthode de Runge-Kutta explicite d'ordre 2 pour 3 populations
def RungeKutta2P3(alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa, h=0.01, v0=np.array([[0],[0],[0]]), N=10000):
    t = np.arange(0, (N+0.5)*h, h)
    V = np.zeros((3, N+1))
    V[:, 0] = v0[:, 0]
    for i in range(1, N+1) :
        vi1 = V[:, i-1].reshape((3, 1))
        k1 = FLVdP3(vi1, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa).reshape((3, 1))
        vi2 = vi1 + h/2 * k1
        k2 = FLVdP3(vi2, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa).reshape((3, 1))
        V[:, i] = (vi1 + h * k2).flatten()
    return t, V


# Implémentation de la méthode de Runge-Kutta explicite d'ordre 4 pour 3 populations
def RungeKutta4P3(alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa, h=0.01, v0=np.array([[0],[0],[0]]), N=10000):
    t = np.arange(0, (N+0.5)*h, h)
    V = np.zeros((3, N+1))
    V[:, 0] = v0[:, 0]
    for i in range(1, N+1) :
        vi1 = V[:, i-1].reshape((3, 1))
        k1 = FLVdP3(vi1, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa).reshape((3, 1))
        vi2 = vi1 + h/2 * k1
        k2 = FLVdP3(vi2, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa).reshape((3, 1))
        vi3 = vi1 + h/2 * k2
        k3 = FLVdP3(vi3, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa).reshape((3, 1))
        vi4 = vi1 + h * k3
        k4 = FLVdP3(vi4, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa).reshape((3, 1))
        V[:, i] = (vi1 + h/6 * (k1 + 2*k2 + 2*k3 + k4)).flatten()
    return t, V


# Fonction pour tracer l'évolution des populations en fonction du temps et des populations entre elles, lorsqu'il n'y a que 2 populations
def plotResultsP3(t, v, p="", n="") :
    plt.figure()
    fig, ax = plt.subplots()
    for i in range(1, 4) :
        ax.plot(t, v[i-1,:], label="Population n°"+str(i))
    ax.set_xlabel("Temps", fontsize=fontSize)
    ax.set_ylabel("Populations", fontsize=fontSize)
    ax.set_title("Évolution des 3 différentes populations en fonction du temps"+p, fontsize=fontSizeTitle)
    ax.xaxis.set_tick_params(labelsize=fontSize)
    ax.yaxis.set_tick_params(labelsize=fontSize)
    ax.legend(loc="upper right", fontsize=fontSize)
    fig.set_figheight(figHeight)
    fig.set_figwidth(figWidth)
    path = currentDirectoryPath + "/graphes/" + n[1] + " populations/relatif"
    os.makedirs(path, exist_ok=True)
    plt.savefig(path + "/" + n[0:2] + "Relatif" + n[2:] + ".png")
    plt.close()
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    cmapRB = colors.LinearSegmentedColormap.from_list("red_blue", ["#FF0000", "#0000FF"])
    ax.scatter(v[0,:], v[1,:], v[2,:], c=v[0,:], cmap=cmapRB, marker="o")
    ax.set_xlabel("\nPopulation n°1", fontsize=fontSize)
    ax.set_ylabel("\nPopulation n°2", fontsize=fontSize)
    ax.set_zlabel("\nPopulation n°3", fontsize=fontSize)
    ax.set_title("Évolution de chacune des 3 populations en fonction des autres"+p, fontsize=fontSizeTitle)
    ax.xaxis.set_tick_params(labelsize=fontSize)
    ax.yaxis.set_tick_params(labelsize=fontSize)
    ax.zaxis.set_tick_params(labelsize=fontSize)
    fig.set_figheight(figHeight)
    fig.set_figwidth(figWidth)
    path = currentDirectoryPath + "/graphes/" + n[1] + " populations/absolu"
    os.makedirs(path, exist_ok=True)
    fig.savefig(path + "/" + n[0:2] + "Absolu" + n[2:] + ".png")
    plt.close()

#=================================================================



#========================[ Améliorations ]========================

# Fonction similaire à FLVdP2 mais qui rajoute un terme aléatoire dans l'évolution soit des proies soit des prédateurs
def FLVdP2Rand(which, zn, alpha, beta, gamma, delta, sigma) :
    F = FLVdP2(zn, alpha, beta, gamma, delta)
    i = 0 if which == "prey" else 1
    F[i] += sigma * zn[i] * np.random.normal()
    # donc soit "Fx += σ * xn * r", soit "Fy += σ * yn * r", r étant la réalisation d'une variable aléatoire suivant une loi normale centrée et réduite
    return F.reshape((2, 1))


# Fonction similaire à RungeKutta4P2 mais qui rajoute un terme aléatoire dans l'évolution des proies puis dans l'évolution des prédateurs
def RungeKutta4P2Rand(alpha, beta, gamma, delta, sigma, h=0.01, z0=np.array([[0],[0]]), N=10000) :
    t = np.arange(0, (N+0.5)*h, h)
    ZPrey = np.zeros((2, N+1))
    ZPredator = np.zeros((2, N+1))
    ZPrey[:, 0] = z0[:, 0]
    ZPredator[:, 0] = z0[:, 0]
    for i in range(1, N+1) :
        # Pour les proies
        zi1 = ZPrey[:, i-1].reshape((2, 1))
        k1 = FLVdP2Rand("prey", zi1, alpha, beta, gamma, delta, sigma).reshape((2, 1))
        zi2 = zi1 + h/2 * k1
        k2 = FLVdP2Rand("prey", zi2, alpha, beta, gamma, delta, sigma).reshape((2, 1))
        zi3 = zi1 + h/2 * k2
        k3 = FLVdP2Rand("prey", zi3, alpha, beta, gamma, delta, sigma).reshape((2, 1))
        zi4 = zi1 + h * k3
        k4 = FLVdP2Rand("prey", zi4, alpha, beta, gamma, delta, sigma).reshape((2, 1))
        ZPrey[:, i] = (zi1 + h/6 * (k1 + 2*k2 + 2*k3 + k4)).flatten()
        # Pour les prédateurs
        zi1 = ZPredator[:, i-1].reshape((2, 1))
        k1 = FLVdP2Rand("predator", zi1, alpha, beta, gamma, delta, sigma).reshape((2, 1))
        zi2 = zi1 + h/2 * k1
        k2 = FLVdP2Rand("predator", zi2, alpha, beta, gamma, delta, sigma).reshape((2, 1))
        zi3 = zi1 + h/2 * k2
        k3 = FLVdP2Rand("predator", zi3, alpha, beta, gamma, delta, sigma).reshape((2, 1))
        zi4 = zi1 + h * k3
        k4 = FLVdP2Rand("predator", zi4, alpha, beta, gamma, delta, sigma).reshape((2, 1))
        ZPredator[:, i] = (zi1 + h/6 * (k1 + 2*k2 + 2*k3 + k4)).flatten()
    return t, ZPrey, ZPredator


# Fonction similaire à FLVdP3 mais qui rajoute un terme représentant la mort naturelle de la troisième population
def FLVdP3Modif1(vn, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa, mu) :
    F = FLVdP3(vn, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa)
    F[2] += -mu * vn[2] * vn[2]
    # donc "Fz += -μ * (zn)²"
    return F.reshape((3, 1))


# Fonction similaire à FLVdP3 mais qui rajoute un terme représentant la prédation de la deuxième population sur la première
def FLVdP3Modif2(vn, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa, sigma) :
    F = FLVdP3(vn, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa)
    F[0] += -sigma * vn[0] * vn[1]
    # donc "Fx += -σ * xn * yn"
    return F.reshape((3, 1))


# Fonction similaire à FLVdP3 mais qui rajoute deux termes représentant, parmi les deux premières populations, l'effet d'une population sur la croissance de l'autre
def FLVdP3Modif3(vn, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa, rho1, rho2) :
    F = FLVdP3(vn, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa)
    F[0] += rho1 * vn[1] * vn[0]
    F[1] += rho2 * vn[0] * vn[1]
    # donc "Fx += ρ1 * yn * xn" et "Fy += ρ2 * xn * yn"
    return F.reshape((3, 1))


def RungeKutta4P3Modifs(alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa, mu, sigma, rho1, rho2, h=0.01, v0=np.array([[0],[0],[0]]), err=1e-8, N=10000):
    t = np.arange(0, (N+0.5)*h, h)
    VModif1 = np.zeros((3, N+1))
    VModif1[:, 0] = v0[:, 0]
    VModif2 = np.zeros((3, N+1))
    VModif2[:, 0] = v0[:, 0]
    VModif3 = np.zeros((3, N+1))
    VModif3[:, 0] = v0[:, 0]
    for i in range(1, N+1) :
        # Pour la première modification
        vi1 = VModif1[:, i-1].reshape((3, 1))
        k1 = FLVdP3Modif1(vi1, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa, mu).reshape((3, 1))
        vi2 = vi1 + h/2 * k1
        k2 = FLVdP3Modif1(vi2, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa, mu).reshape((3, 1))
        vi3 = vi1 + h/2 * k2
        k3 = FLVdP3Modif1(vi3, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa, mu).reshape((3, 1))
        vi4 = vi1 + h * k3
        k4 = FLVdP3Modif1(vi4, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa, mu).reshape((3, 1))
        VModif1[:, i] = (vi1 + h/6 * (k1 + 2*k2 + 2*k3 + k4)).flatten()
        # Pour la deuxième modification
        vi1 = VModif2[:, i-1].reshape((3, 1))
        k1 = FLVdP3Modif2(vi1, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa, sigma).reshape((3, 1))
        vi2 = vi1 + h/2 * k1
        k2 = FLVdP3Modif2(vi2, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa, sigma).reshape((3, 1))
        vi3 = vi1 + h/2 * k2
        k3 = FLVdP3Modif2(vi3, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa, sigma).reshape((3, 1))
        vi4 = vi1 + h * k3
        k4 = FLVdP3Modif2(vi4, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa, sigma).reshape((3, 1))
        VModif2[:, i] = (vi1 + h/6 * (k1 + 2*k2 + 2*k3 + k4)).flatten()
        # Pour la troisième modification
        vi1 = VModif3[:, i-1].reshape((3, 1))
        k1 = FLVdP3Modif3(vi1, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa, rho1, rho2).reshape((3, 1))
        vi2 = vi1 + h/2 * k1
        k2 = FLVdP3Modif3(vi2, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa, rho1, rho2).reshape((3, 1))
        vi3 = vi1 + h/2 * k2
        k3 = FLVdP3Modif3(vi3, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa, rho1, rho2).reshape((3, 1))
        vi4 = vi1 + h * k3
        k4 = FLVdP3Modif3(vi4, alpha, beta, gamma, delta, epsilon, zeta, eta, theta, iota, kappa, rho1, rho2).reshape((3, 1))
        VModif3[:, i] = (vi1 + h/6 * (k1 + 2*k2 + 2*k3 + k4)).flatten()
    return t, VModif1, VModif2, VModif3
    

#=================================================================



#============================[ Autre ]============================

# Fonction qui enlève la partie décimale d'un nombre si elle est nulle
def verifInt(a) :
    if float.is_integer(a) :
        return int(a)
    return a


# Fonction qui renvoie les valeurs spécifiques d'un graphique pour un nombre quelconque de populations
def chaineValeurs(z0, pn, pv, n=2, b="") :
    if n == 2 :
        l = "z"
    elif n == 3 :
        l = "v"
    s = "[" + l + "0=("
    n = len(z0)
    for i in range(n) :
        s += str(verifInt(float(str(z0[i]).strip("[]"))))
        if i == n - 1 :
            s += "); "
        else :
            s += ";"
    m = len(pn)
    for i in range(m) :
        s += str(pn[i]) + "=" + str(verifInt(float(str(pv[i]).strip("[]"))))
        if i < m - 1 :
            s += "; "
    s += b + "]"
    return s

#=================================================================


#=====================[ fonction principale ]=====================

def main() :
    # Pour 2 populations
    plt.close("all")
    parametresNomsP2 = ["α", "β", "γ", "δ"]
    parametresValeursP2 = np.array([[0.2 , 1, 0.8, 0.9],
                                    [0.15, 1, 1.2, 0.2],
                                    [0.3 , 2, 2.5, 0.7],
                                    [0.1 , 1, 2  , 1  ]])
    initialisationsP2 = np.array([[1, 10, 4, 3, 2],
                                  [2, 8 , 1, 3, 5]])
        # Coefficients qui ne doivent vraiment pas être trop grands !
    NparamsP2 = np.shape(parametresValeursP2)[1]
    Nz0P2 = np.shape(initialisationsP2)[1]
    for i in range(NparamsP2) :
        pv = parametresValeursP2[:, i].reshape((4, 1))
        for j in range(Nz0P2) :
            z0 = initialisationsP2[:, j].reshape((2, 1))
            s = chaineValeurs(z0, parametresNomsP2, pv)
            t, z = EulerImpliciteP2(pv[0], pv[1], pv[2], pv[3], z0=z0)
            plotResultsP2(t, z, "\n(à l'aide de la méthode d'Euler implicite)\n"+s, "P2EulerN"+str(i*(NparamsP2+1)+j+1))
            t, z = RungeKutta2P2(pv[0], pv[1], pv[2], pv[3], z0=z0)
            plotResultsP2(t, z, "\n(à l'aide de la méthode de Runge-Kutta explicite d'ordre 2)\n"+s, "P2RK2N"+str(i*(NparamsP2+1)+j+1))
            t, z = RungeKutta4P2(pv[0], pv[1], pv[2], pv[3], z0=z0)
            plotResultsP2(t, z, "\n(à l'aide de la méthode de Runge-Kutta explicite d'ordre 4)\n"+s, "P2RK4N"+str(i*(NparamsP2+1)+j+1))
    
    # Pour 3 populations
    parametresNomsP3 = ["α", "β", "γ", "δ", "ε", "ζ", "η", "θ", "ι", "κ"]
    parametresValeursP3 = np.array([[3      ,   1.5  , 0.5  , 0.25],
                                    [0.025  ,   0.025, 1    , 0.05],
                                    [0.1    ,   0.1  , 0.6  , 1.1 ],
                                    [2      ,   1.1  , 1.4  , 0.8 ],
                                    [0.05   ,   0.05 , 0.025, 0.05],
                                    [0.025  ,   0.025, 0.05 , 1   ],
                                    [0.05   ,   0.05 , 0.3  , 0.1 ],
                                    [1      ,   0.7  , 0.7  , 1.5 ],
                                    [0.05   ,   0.05 , 0.1  , 0.7 ],
                                    [0.05   ,   0.05 , 1.25 , 1.2 ]])
    initialisationsP3 = np.array([[1, 3, 4, 3, 2],
                                  [1, 1, 2, 4, 2],
                                  [1, 4, 1, 2, 4]])
        # Coefficients qui ne doivent encore moins être trop grands !
    NparamsP3 = np.shape(parametresValeursP3)[1]
    Nv0P3 = np.shape(initialisationsP3)[1]
    for i in range(NparamsP3) :
        pv = parametresValeursP3[:, i].reshape((10, 1))
        for j in range(Nv0P3) :
            v0 = initialisationsP3[:, j].reshape((3, 1))
            s = chaineValeurs(v0, parametresNomsP3, pv, 3)
            t, v = EulerImpliciteP3(pv[0], pv[1], pv[2], pv[3], pv[4], pv[5], pv[6], pv[7], pv[8], pv[9], v0=v0)
            plotResultsP3(t, v, "\n(à l'aide de la méthode d'Euler implicite)\n"+s, "P3EulerN"+str(i*(NparamsP3+1)+j+1))
            t, v = RungeKutta2P3(pv[0], pv[1], pv[2], pv[3], pv[4], pv[5], pv[6], pv[7], pv[8], pv[9], v0=v0)
            plotResultsP3(t, v, "\n(à l'aide de la méthode de Runge-Kutta explicite d'ordre 2)\n"+s, "P3RK2N"+str(i*(NparamsP3+1)+j+1))
            t, v = RungeKutta4P3(pv[0], pv[1], pv[2], pv[3], pv[4], pv[5], pv[6], pv[7], pv[8], pv[9], v0=v0)
            plotResultsP3(t, v, "\n(à l'aide de la méthode de Runge-Kutta explicite d'ordre 4)\n"+s, "P3RK4N"+str(i*(NparamsP3+1)+j+1))
    
    # Pour les améliorations avec 2 populations
    pv = parametresValeursP2[:, 0].reshape((4, 1))
    z0 = initialisationsP2[:, 0].reshape((2, 1))
    sigma = 2
    for i in range(4) :
        t, zrey, zredator = RungeKutta4P2Rand(pv[0], pv[1], pv[2], pv[3], 2, z0=z0)
        s = chaineValeurs(z0, parametresNomsP2, pv, 2, "; σ="+str(sigma))
        plotResultsP2(t, zrey, ", avec modification\nde l'évolution des proies (à l'aide de la méthode de Runge-Kutta explicite d'ordre 4)\n"+s, "P2RK4ModifPreyN"+str(i+1))
        plotResultsP2(t, zredator, ", avec modification\nde l'évolution des prédateurs (à l'aide de la méthode de Runge-Kutta explicite d'ordre 4)\n"+s, "P2RK4ModifPredatorN"+str(i+1))
    
    # Pour les améliorations avec 3 populations
    pv = parametresValeursP3[:, 0].reshape((10, 1))
    v0 = initialisationsP3[:, 0].reshape((3, 1))
    mu = [0.05, 0.2, 0.5] 
    sigma = [0.1, 0.4, 0.8]
    rho1 = [0.3, 0.1, 0.05]
    rho2 = [0.05, 0.1, 0.3]
    for i in range(3) :
        t, vodif1, vodif2, vodif3 = RungeKutta4P3Modifs(pv[0], pv[1], pv[2], pv[3], pv[4], pv[5], pv[6], pv[7], pv[8], pv[9], mu[i], sigma[i], rho1[i], rho2[i], v0=v0)
        s = chaineValeurs(v0, parametresNomsP3, pv, 3, "; μ="+str(mu[i]))
        plotResultsP3(t, vodif1, " avec implémentation\nde la modification n°1 (à l'aide de la méthode de Runge-Kutta explicite d'ordre 4)\n"+s, "P3RK4Modif1N"+str(i+1))
        s = chaineValeurs(v0, parametresNomsP3, pv, 3, "; σ="+str(sigma[i]))
        plotResultsP3(t, vodif2, " avec implémentation\nde la modification n°2 (à l'aide de la méthode de Runge-Kutta explicite d'ordre 4)\n"+s, "P3RK4Modif2N"+str(i+1))
        s = chaineValeurs(v0, parametresNomsP3, pv, 3, "; ρ$_{1}$="+str(rho1[i])+"; ρ$_{2}$="+str(rho2[i]))
        plotResultsP3(t, vodif3, " avec implémentation\nde la modification n°3 (à l'aide de la méthode de Runge-Kutta explicite d'ordre 4)\n"+s, "P3RK4Modif3N"+str(i+1)) 
    

main()

#=================================================================
